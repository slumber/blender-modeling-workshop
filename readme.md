# Workshop : Collaborative modeling in Blender

## Summary

The objective of this workshop is to teach students 3D modeling on Blender. Blender is an open source software used in many industries from animation to medical to create and process synthetic images. This workshop will have an important collaborative dimension, as the learners will be in the same virtual space and will work together to create a collective work

## Background

During my PhD thesis, I developed a real-time collaboration solution for digital creation in Blender. By connecting to an online session, artists build and interact in the same 3D scene and in the same temporality. Applied to industry, this method makes artists aware of the work in progress on the stage and parallelizes the creation of the different stages of fabrication. Applied to computer graphics teaching, real-time collaboration puts students and teachers in the same virtual space. This proximity favors mutual aid and allows the teacher to efficiently detect students in difficulty and help them.

Moreover, we observed that this method reduces the distance between theory and practice. It thus facilitates the assimilation of the concepts taught.

## Program

The workshop will last 18 hours and assumes that the students have never used Blender, however, the program is easily adaptable to the level of the students that will be proposed. The proposed program is as follows:

Day 1 :
- 3 hours : Introduction to the Blender interface, familiarization with the 3D viewport (Theory).
- 3 hours: Introduction to 3D modeling tools, individual modeling of a simple object (Theory + Practice)

- 3 hours : Introduction to procedural modeling tools, individual modeling of a simple object (Theory + Practice)
- 3 hours : Introduction to the real-time collaboration workflow in Blender and start of the collective project (Theoretical + Practical)
- 3 hours : collaborative project, creation of a 3D scene (Practice)
- 3 hours : collaborative project, creation of a 3D scene (Practice) 